const app = new Vue({
    el: '#app',
    data: {
        titulo: 'Texto invertido',
        mensajes: [],
        nuevoMensaje: ''

    },
    methods: {
        agregarMensaje: function(){
            console.log('diste click', this.nuevoMensaje)

            this.mensajes.push({
                nombre:  this.nuevoMensaje ,
                sinParentesis: this.nuevoMensaje.replace(/[{()}]/g, ''),
                invertido: this.nuevoMensaje.replace(/[{()}]/g, '').split('').reverse().join(''),
                estado: false
            });
            console.log(this.mensajes)
            this.nuevoMensaje = ''
        }
    },
})